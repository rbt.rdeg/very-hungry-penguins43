(** The Graphical User Interface module, with all GTK handling *)

(** This is the main function of the graphical user interface. *)
val main : unit -> unit
